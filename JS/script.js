

let isComputer;
let isPlaying = false;
let isX = true;
let p1ScoreValue = 0;
let p2ScoreValue = 0;
let drawCount = 0;
let result;
let isWinnerFound = false;
let winner;
let p1Name, p2Name;

let info = document.getElementById("info");
let p1Score = document.getElementById("p1Score");
let p2Score = document.getElementById("p2Score");
let btnStartReset = document.getElementById("startReset");

let possibilites = 	[
						["cell0", "cell1", "cell2"],
						["cell3", "cell4", "cell5"],
						["cell6", "cell7", "cell8"],
						["cell0", "cell3", "cell6"],
						["cell1", "cell4", "cell7"],
						["cell2", "cell5", "cell8"],
						["cell0", "cell4", "cell8"],
						["cell2", "cell4", "cell6"]
					];

// Event Registrations

document.querySelectorAll(".cell").forEach(function(element){
	element.addEventListener('click', processClick);
});
btnStartReset.addEventListener('click', styleClick);
btnStartReset.addEventListener('mouseenter', styleMouseEnter);
btnStartReset.addEventListener('mouseleave', styleMouseLeave);
myStorage = window.sessionStorage;

function init(){ 
	isComputer = myStorage.getItem('isComputer');
	p1Name = myStorage.getItem('Player1');
	p2Name = myStorage.getItem('Player2');
}
// this getItem will work everytime as this page is not supposed to open independently!

init();

function showGrid(){
	document.getElementById("grid").style.display = "grid";
	document.getElementById("gridbox").style.display = "block";
	document.getElementById("info").style.display = "none";
	document.querySelectorAll(".cell").forEach(function(element){
		element.innerHTML = "";
	});
}

function showInfo(){

	info.style.display = "block";
	document.getElementById("grid").style.display = "none";
	document.getElementById("gridbox").style.display = "none";
	if(isWinnerFound){
		info.innerHTML = winner + " Won";
	} else {
		info.innerHTML = "Draw: " +drawCount;
	}
}

function processClick(e){
	if(isPlaying && !isWinnerFound){
		if(e.target.innerHTML == ""){
			if(isX){
				e.target.innerHTML = 'X';
			} else {
				e.target.innerHTML = 'O';
			}
			isX = !isX;
			checkMatch(e);
		}

	} else {
		btnStartReset.style.transition = "all .3s";
		btnStartReset.style.borderColor = "#EF3054";
		setTimeout(function(){
			btnStartReset.style.borderColor = "#00FFFF";
		}, 300);

		setTimeout(function(){
			btnStartReset.style.borderColor = "#EF3054";
		}, 500);

		setTimeout(function(){
			btnStartReset.style.borderColor = "#00FFFF";
		}, 1200);
		
		setTimeout(function(){
			btnStartReset.style.transition = "all .1s";
		}, 1300);
	}
}

function styleMouseEnter(e){
	e.target.style.color = "#00FF29";
	e.target.style.borderColor = "#00FF29";
}

function styleMouseLeave(e){
	e.target.style.color = "#00FFFF";
	e.target.style.borderColor = "#00FFFF";
}

function styleClick(){
	isPlaying = !isPlaying;
	showGrid();
	if(isPlaying){
		// pressed start
		btnStartReset.innerHTML = "Reset";
	} else {
		//pressed reset
		btnStartReset.innerHTML = "Start";
		isX = true;
		isWinnerFound = false;
		// clear all the grid-cells
		document.querySelectorAll(".cell").forEach(function(cell){
			cell.innerHTML = "";
		});
	}
	
}

function checkMatch(e){
		
		id = e.target.id;
		possibilites.forEach(function(possibleWin){

			let i, xCount = 0, oCount = 0;
			for(i = 0; i<possibleWin.length; i++){
				let cell = document.getElementById(possibleWin[i]);
				if(cell.innerHTML != ""){
					if(cell.innerHTML == 'X'){
						xCount++;
					} else if(cell.innerHTML == 'O') {
						oCount++;
					}
				}
			}
			if(xCount==3 && i==3){
				p1ScoreValue++;
				isWinnerFound = true;
				winner = p1Name;
				if(p1ScoreValue<10){
					p1Score.innerHTML = "0"+p1ScoreValue;
				} else{
					p1Score.innerHTML = p1ScoreValue;
				}
				setTimeout(function(){
					showInfo();
				}, 200);
			} else if(oCount==3 && i==3){
				p2ScoreValue++;
				isWinnerFound = true;
				winner = p2Name;
				if(p2ScoreValue<10){
					p2Score.innerHTML = "0"+p2ScoreValue;
				} else{
					p2Score.innerHTML = p2ScoreValue;
				}
				setTimeout(function(){
					showInfo();
				}, 200);
			}

		});

		let count = 0;
		document.querySelectorAll(".cell").forEach(function(element){
			if(!(element.innerHTML == "")){
				count++;
			}
		});
		if(count == 9 && !isWinnerFound){
			drawCount++;
			setTimeout(function(){
				showInfo();
			}, 200);
		}
}