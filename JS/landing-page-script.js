
let isComputer = false;
var isPlaying = false;
myStorage = window.sessionStorage;

// fetching elements
let pvpMode = document.getElementById("pvp");
let computerMode = document.getElementById("computer");
let p1Input = document.getElementById("player1input");
let p2Input = document.getElementById("player2input");
let btnPlay = document.getElementById("play");

// Event Registration
if(!isPlaying){
	btnPlay.addEventListener('click', startGame);
	pvpMode.addEventListener('click', setMode);
	computerMode.addEventListener('click', setMode);
	pvpMode.addEventListener('mouseenter', styleMouseEnter);
	computerMode.addEventListener('mouseenter', styleMouseEnter);
	pvpMode.addEventListener('mouseleave', styleMouseLeave);
	computerMode.addEventListener('mouseleave', styleMouseLeave);
	p1Input.addEventListener('focusin', styleFocusIn);
	p2Input.addEventListener('focusin', styleFocusIn);
	p1Input.addEventListener('focusout', styleFocusOut);
	p2Input.addEventListener('focusout', styleFocusOut);
	p1Input.addEventListener('click', clearInput);
	p2Input.addEventListener('click', clearInput);
}

function setMode(e){
	if(e.target == pvpMode){
		// pressed 1v1
        isComputer = false;
		p2Input.value = "Player 2";
		p2Input.readOnly = false;
		pvpMode.style.color = "#00FF29";
		pvpMode.style.border = "2px solid #00FF29";
		computerMode.style.color = " #00FFFF";
		computerMode.style.border = "2px solid rgba(0, 0, 0, 0)";
	} else {
		// pressed computer
		isComputer = true;
		p2Input.readOnly = true;
		p2Input.value = "Computer";
		computerMode.style.color = " #00FF29";
		computerMode.style.border = "2px solid #00FF29";
		pvpMode.style.color = "#00FFFF";
		pvpMode.style.border = "2px solid rgba(0, 0, 0, 0)";
	}
}

function styleFocusIn(e){
		// p1Input is Selected
		e.target.style.color = "#00FF29";
		e.target.style.borderBottomColor =  "#00FF29";
}

function styleFocusOut(e){
	e.target.style.color = "#00FFFF"; 
	e.target.style.borderBottomColor =  "#00FFFF";
	if(e.target.value == ""){
		if(e.target == p1Input){
			p1Input.value = "Player 1";
		} else {
			if(isComputer){
				p2Input.value = "Computer";
			}
			else{
				p2Input.value = "Player 2";
			}
		}
	}
}

function clearInput(e){
	if(e.target.value == 'Player 1' || e.target.value == 'Player 2'){
		e.target.value = "";
	}
}

function styleMouseEnter(e){
	e.target.style.color = "#00FF29";
}

function styleMouseLeave(e){
	e.target.style.color = "#00FFFF";
}

function startGame(){
	myStorage.setItem('isComputer', isComputer);
	myStorage.setItem('Player1', p1Input.value);
	myStorage.setItem('Player2', p2Input.value);
	window.location.href = "./index.html";
}